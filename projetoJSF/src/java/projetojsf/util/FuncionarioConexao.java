/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetojsf.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Dayana
 */
public class FuncionarioConexao {
    
    private static Connection conexao;
    private static final String URL_CONEXAO = "jdbc:postgresql://127.0.0.1:5432/postgres";
    private static final String USUARIO = "postgres";
    private static final String SENHA = "2012";

    public static Connection getConexao() {
        if(conexao == null){
            try {
                // Certificando que o driver está funcionando
                Class.forName("org.postgresql.Driver");
                conexao = DriverManager.getConnection(URL_CONEXAO, USUARIO, SENHA);
            } catch (SQLException ex) {
                Logger.getLogger(FuncionarioConexao.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(FuncionarioConexao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return conexao;
    }
    
    public static void fecharConexao(){
        if(conexao != null){
            try{
                conexao.close();
                conexao = null;
            } catch(SQLException ex){
                Logger.getLogger(FuncionarioConexao.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
  
}
   

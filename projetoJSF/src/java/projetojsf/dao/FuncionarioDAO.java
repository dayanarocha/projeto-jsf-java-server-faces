/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetojsf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import projetojsf.entidade.Funcionario;
import projetojsf.util.FuncionarioConexao;

/**
 *
 * @author Dayana
 */
public class FuncionarioDAO {
    
    public void salvar(Funcionario funcionario){
        try {
            Connection conexao = FuncionarioConexao.getConexao();
            PreparedStatement ps = conexao.prepareCall("INSERT INTO funcionario(cpf,nome,cargo) VALUES (?,?,?);");
            ps.setString(1, funcionario.getCpf());
            ps.setString(2, funcionario.getNome());
            ps.setString(3, funcionario.getCargo());
             // Nao usa o executeSQL porque nao tem retorno ja que nao é select
            ps.execute();
            FuncionarioConexao.fecharConexao();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public List<Funcionario> buscar(){
        Connection conexao = FuncionarioConexao.getConexao();
        try {
            PreparedStatement ps = conexao.prepareStatement("SELECT * FROM funcionario;");
            ResultSet resultSet = ps.executeQuery();
            List<Funcionario> funcionarios = new ArrayList<>();
            
            while(resultSet.next()){
                // Setando os valores gravados no banco para os objetos na memoria
                Funcionario funcionario = new Funcionario();
                funcionario.setCpf(resultSet.getString("cpf"));
                funcionario.setNome(resultSet.getString("nome"));
                funcionario.setCargo(resultSet.getString("cargo"));
                funcionarios.add(funcionario);
            }
            return funcionarios;
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void atualizar(Funcionario funcionario){
        try {
            Connection conexao = FuncionarioConexao.getConexao();
            PreparedStatement ps = conexao.prepareCall("UPDATE funcionario SET nome=?,cargo=? WHERE cpf=?;");
            ps.setString(1, funcionario.getNome());
            ps.setString(2, funcionario.getCargo());
            ps.setString(3, funcionario.getCpf());
             // Nao usa o executeSQL porque nao tem retorno ja que nao é select
            ps.execute();
            FuncionarioConexao.fecharConexao();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void remover(Funcionario funcionario){
        try {
            Connection conexao = FuncionarioConexao.getConexao();
            PreparedStatement ps = conexao.prepareCall("DELETE FROM funcionario WHERE cpf=?;");
            ps.setString(1, funcionario.getCpf());
             // Nao usa o executeSQL porque nao tem retorno ja que nao é select
            ps.execute();
            FuncionarioConexao.fecharConexao();
        } catch (SQLException ex) {
            Logger.getLogger(FuncionarioDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

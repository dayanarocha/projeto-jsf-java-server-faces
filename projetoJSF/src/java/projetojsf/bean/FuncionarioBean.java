/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projetojsf.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import projetojsf.dao.FuncionarioDAO;
import projetojsf.entidade.Funcionario;

/**
 *
 * @author Dayana
 */
@SessionScoped
@ManagedBean(name = "funcionarioBean")
public class FuncionarioBean {
    
    private Funcionario funcionario = new Funcionario();
    private List<Funcionario> funcionarios = new ArrayList<>();
    private FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
    
    public void adicionar(){
        funcionarios.add(funcionario);
        new FuncionarioDAO().salvar(funcionario);
        funcionario = new Funcionario();
        this.listar();
    }
    
    public void listar(){
        funcionarios = funcionarioDAO.buscar();
    }
    
    public void editar(Funcionario f){
        // Vai carregar no formulário os dados da linha da tabela
        funcionario = f;
    }
    
    public void atualizar(){
        new FuncionarioDAO().atualizar(funcionario);
        funcionario = new Funcionario();
    }
    
    public void remover(Funcionario f){
        new FuncionarioDAO().remover(f);
        funcionarios.remove(f);
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public List<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }
    
}
